#!/usr/bin/lua5.3

local sprintf=string.format
local push=table.insert
local join=table.concat
local sbyte=string.byte

function vis(val)  
    if type(val)~="string" then  
        return tostring(val)  
    end  
    return '"'..val..'"'  
end

function vist(val)
	local did={}
	local function vt(v)
		if type(v)~="table" then return vis(v) end
		if did[v] then return did[v] end
		did[v]=tostring(v)
		local r={}
		local dk={}
		for i=1,#v do
			r[#r+1]=vt(v[i])
			dk[i]=true
		end
		local ks={}
		for k in pairs(v) do
			if not dk[k] then ks[#ks+1]=k end
		end
		table.sort(ks,cmp)
		for _,k in ipairs(ks) do
			if type(k)=="string" and k:match("^[_a-zA-Z][_a-zA-Z0-9]*$") then
				r[#r+1]=sprintf("%s=%s",k,vt(v[k]))
			else
				r[#r+1]=sprintf("[%s]=%s",vis(k),vt(v[k]))
			end
		end
		return '{'..join(r,',')..'}'
	end
	return vt(val)
end

