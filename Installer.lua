local component = require("component")
local fs = component.filesystem
local shell=require("shell")

local sprintf=string.format
local push=table.insert
local join=table.concat
local sbyte=string.byte

local LIBRARY_PATH="/home/lib"
local APPLICATION_PATH="/home/programs"

local M={}
M.Info=
{
    type="app",
    source={
        user="LordFuchur",
        page="gitlab.com",
        project="oclualib",
        type="oclualib"
    },
    files={
        "Installer.lua"
    },
    dependencies={
    }
}

local function buildURLFromSource(sourceTable)
    local TEMPLATE=[[https://{{page}}/{{user}}/{{project}}/]]
    local ret
    if(sourceTable.page == "gitlab.com") then
      ret = TEMPLATE:gsub("{{(%w+)}}",sourceTable)
      ret = ret .. "raw/master/"
    elseif(sourceTable.page == "github.com") then
        sourceTable.page = "raw.githubusercontent.com"
      ret = TEMPLATE:gsub("{{(%w+)}}",sourceTable)
      ret = ret .. "master/"
    else
      return nil, "Unknown page"
    end
    return ret
end

local function wget(src,dest)
    dest=dest or ""
    local cmd=sprintf("wget %s %s",src,dest)
    return shell.execute(cmd)
end

local function getJsonLib()
    if not fs.isDirectory(LIBRARY_PATH) then
        fs.makeDirectory(LIBRARY_PATH)
    end
    if not fs.isDirectory(LIBRARY_PATH.."/json") then
        fs.makeDirectory(LIBRARY_PATH.."/json")
    end
    wget("https://raw.githubusercontent.com/rxi/json.lua/master/json.lua",LIBRARY_PATH.."/json/json.lua")
end

local function getProjectFromJson(jsonFile)
    -- read jsonfile into variable
    local fd,err=assert(io.open(jsonFile,"rb"),"Failed to open json file!")
    local file=fd.read("*a")
    fd.close()
    -- load json library
    local json=require("json.json")
    -- decode json to lua table
    local jsonTbl=json.decode(file)
    -- get source url
    local baseurl=nil
    if jsonTbl and jsonTbl.source and type(jsonTbl.source)=="table" then
        baseurl=buildURLFromSource(jsonTbl.source)
    else
        return nil,"Json file doesnt contain source table!"
    end
    -- check files
    if jsonTbl and jsonTbl.files and type(jsonTbl.files)=="table" then
        if #jsonTbl.files>0 then
            local dest=""
            local src=baseurl
            if jsonTbl.type and jsonTbl.type=="lib" then
                dest=LIBRARY_PATH.."/"..jsonTbl.source.project
            elseif jsonTbl.type and jsonTbl.type=="app" then
                dest=APPLICATION_PATH.."/"..jsonTbl.source.project
            else
                dest="/home"
            end
            -- create folder
            if not fs.isDirectory(dest) then
                print(sprintf("Create %s\n",dest))
                fs.makeDirectory(dest)
            end                        
            -- download project files
            for _,projFile in ipairs(jsonTbl.files) do
                src=src.."/"..projFile
                dest=dest.."/"..projFile
                wget(src,dest)
            end
            -- download dependencies files
            if jsonTbl.dependencies and #jsonTbl.dependencies>0 then
                for _,dep in ipairs(jsonTbl.dependencies) do
                    local tmpname=os.tmpname()
                    wget(dep,tmpname)
                    getProjectFromJson(tmpname)
                end
            end
        else
            return nil,"No files to download!"
        end
    else
        return nil,"Json file doesnt contain files table!"
    end
end
M.DownloadFromJson=getProjectFromJson

local function init()
    getJsonLib()
end
M.Init=init

return M